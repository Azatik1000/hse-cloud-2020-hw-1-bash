#!/bin/bash

read expression
printf "%.3f" $(bc <<< "scale=4; $expression")
