#!/bin/bash

read N

sum=0
i=1
while [ "$i" -le "$N" ]; do
    read num
    ((sum += $num))
    i=$(($i + 1))
done

printf "%.3f\n" $(bc <<< "scale=4; $sum/$N")

