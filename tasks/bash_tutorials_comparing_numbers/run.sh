#!/bin/bash
read X
read Y

comparison="is equal to"
if [ "$X" -lt "$Y" ]; then
    comparison="is less than"
elif [ "$X" -gt "$Y" ]; then
    comparison="is greater than"
fi

echo -n "X $comparison Y"
