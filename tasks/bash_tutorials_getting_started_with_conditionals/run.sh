#!/bin/bash

read -n 1 answer

if [ "$answer" = "Y" ] || [ "$answer" = "y" ]; then
    output="YES"
elif [ "$answer" = "N" ] || [ "$answer" = "n" ]; then
    output="NO"    
fi

echo -n $output
