#!/bin/bash

readarray -t array
declare -a filtered=( ${array[@]/*[aA]*/} )
echo "${filtered[@]}"
